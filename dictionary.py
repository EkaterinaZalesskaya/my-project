import sys

def reverse_dictionary(dictionary):
    result = {}
    for key, values in dictionary.items():
        for value in values:
            if value not in result:
                result[value] = [key]
            else:
                result[value].append(key)
    return result

def parse_dictionary(text):
    result = {}
    for line in text.split('\n'):
        line = line.strip()
        if not line:
            continue

        key, value = line.split(' - ')
        value = value.split(', ')
        result[key] = value
    return result

def print_dictionary(dictionary):
    for key in sorted(dictionary):
        print(key + ' - ' + ', '.join(dictionary[key]))


def main():
    text = sys.stdin.read()
    base_dictionary = parse_dictionary(text)
    reversed_dictionary = reverse_dictionary(base_dictionary)
    print_dictionary(reversed_dictionary)