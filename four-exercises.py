def divisible(begin, end):
    result = []
    for num in range(begin, end + 1):
        if (num % 7 == 0) and (num % 5 != 0):
            result.append(str(num))

    return " ".join(result)

def register_count(string):
    result = {"UPPER": 0, "LOWER": 0}
    for s in string:
        if s.isupper():
            result["UPPER"] += 1
        elif s.islower():
            result["LOWER"] += 1
        else:
            pass
    return result

def pairwise_diff(first, second):
    assert len(first) == len(second)
    diff = 0
    for f,s in zip(first, second):
        if f != s:
            diff += 1
    return round(diff / len(first), 2)